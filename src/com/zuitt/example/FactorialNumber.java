package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num;
        int counter = 1;
        int answer = 1;

        System.out.println("Input an integer whose factorial will be computed: ");
        try {
            num = scanner.nextInt();

            if (num >= 0) {
                /* Using While-loop */
                /*counter = num;
                while (counter > 0) {
                    answer *= counter;
                    counter--;
                }*/

                /* Using For loop */
                for (int i = num; i > 0; i--) {
                    answer *= i;
                }

                System.out.println("The factorial of " + num + " is " + answer);
            } else {
                System.out.println("Input should be a positive number.");
            }

        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a number.");
        }
    }
}
